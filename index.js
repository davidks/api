const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
var session = require('./config/session')
var json2xls = require('json2xls')
var app = express({ defaultErrorHandler: false });

var port = process.env.PORT || 2019;

app.use(cors())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(express.static('public'))
app.use(express.static('uploads'))
app.use(session)
app.use(json2xls.middleware)

app.get('/', (req, res) => {
    res.send('<h1>API RUNNING : SUCCESS</h1>')
})

const { authRouter, masterRouter,mediaHelper } = require('./routers');

app.use('/auth', authRouter);
app.use('/master', masterRouter);
app.use('/helper', mediaHelper)




app.listen(port, () => console.log('API Aktif di port ' + port))