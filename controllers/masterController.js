const conn = require('../database');
const laporan_data_sales_model = require("../model/LaporanModel")
var laporan_agent = require('../model/LaporanAgent')



module.exports = {

    masterPrinciple: (req, res) => {
        var sql = `SELECT * FROM principle`

        conn.query(sql, (err, result) => {
            if (err) throw err;
            res.send(result)
        })
    },

    masterUser: (req, res) => {
        var sql = `SELECT user_username, user_name, user_level, area, group_spv FROM user;`
        conn.query(sql, (err, result) => {
            if (err) throw err;
            res.send(result)
        })
    },

    masterArea: (req, res) => {
        var sql = `SELECT * FROM  area`

        conn.query(sql, (err, result) => {
            if (err) throw err;
            res.send(result)
        })
    },

    masterDistributor: (req, res) => {
        var sql = `SELECT * FROM distributors`

        conn.query(sql, (err, result) => {
            if (err) throw err;
            res.send(result)
        })
    },
    addPrinciple: (req,res) => {
        var newPrinciple = req.body;

        var sql = `INSERT INTO principle SET ?;`;   
        conn.query(sql, newPrinciple, (err, result) => {
            console.log(result);
            if(err) {
                throw err;
                // console.log(err);
            }

            res.send(result);
        })
    },
    deletePrinciple: (req,res) => {
        var sql = `DELETE FROM principle where id = '${req.params.id}';`;
        conn.query(sql,(err, result) => {
            if (err) throw err;
            res.send(result);
        })
    },
    editPrinciple: (req,res) => {
        var editPrinciple = req.body
        var sql = `UPDATE principle SET ? where id = '${req.params.id}';`;
        conn.query(sql,editPrinciple,(err, result) => {
            if (err) throw err;
            res.send(result);
        })
    },
    newArea: (req, res) => {
        var newArea = req.body;

        var sql = `INSERT INTO area SET ?;`;   
        conn.query(sql, newArea, (err, result) => {
            console.log(result);
            if(err) {
                throw err;
                // console.log(err);
            }

            res.send(result);
        })
    },
    deleteArea: (req,res) => {
        var sql = `DELETE FROM area where id = '${req.params.id}';`;
        conn.query(sql,(err, result) => {   
            if (err) throw err;
            res.send(result);
        })
    },
    editArea: (req,res) => {
        var editArea = req.body
        var sql = `UPDATE area SET ? where id = '${req.params.id}';`;
        conn.query(sql,editArea,(err, result) => {
            if (err) throw err;
            res.send(result);
        })
    },
    newDistributor: (req,res) => {
        var newDT = req.body;

        var sql = `INSERT INTO distributors SET ?;`;   
        conn.query(sql, newDT, (err, result) => {
            console.log(result);
            if(err) {
                throw err;
                // console.log(err);
            }
            res.send(result);
        })
    },
    deleteDistributor: (req,res) => {
        var sql = `DELETE FROM distributors where id = '${req.params.id}';`;
        conn.query(sql,(err, result) => {
            if (err) throw err;
            res.send(result);
        })
    },

    editDistributor: (req,res) => {
        var editDT = req.body
        var sql = `UPDATE distributors SET ? where id = '${req.params.id}';`;
        conn.query(sql,editDT,(err, result) => {
            if (err) throw err;
            res.send(result);
        })
    },
    addUser: (req, res) => {
        var user_username = req.body.user_username
        var user_name = req.body.user_name
        var user_password = req.body.user_password
        var user_level = req.body.user_level
        var sip = req.body.no_telp
        var firstlogin = req.body.firstlogin
        var area = req.body.area
        var group_spv = req.body.group_spv

        console.log(req.body)

        var sql = `INSERT INTO user 
        (user_username, user_password, user_name, user_level, sip, firstlogin,  area, group_spv,id_asterisk) 
        VALUES (
        ${conn.escape(user_username)},${conn.escape(user_password)},${conn.escape(user_name)},${conn.escape(user_level)},${conn.escape(sip)},${conn.escape(firstlogin)},${conn.escape(area)},${conn.escape(group_spv)},'0')`

        console.log(sql)
        res.send(sql)
        // conn.query(sql,(err,result)=>{
        //     if(err) throw err
        //     res.send(result)
        // })
    }, userDelete: (req, res) => {
        var id = req.body.id_user
        id = conn.escape(id)
        var sql = `DELETE FROM user where user_id = ${id}`;
        conn.query(sql, (err, result) => {
            if (err) throw err
            if (result.affectedRows > 0) {
                res.send({ status: "sukses" })
            } else {
                res.send({ status: "fail" })
            }
        })
    }, userUpdate: (req, res) => {
        var password = req.body.password
        var no_handphone = req.body.no_handphone
        var distributor = req.body.distributor
        distributor = JSON.stringify(distributor)
        var id = req.body.id_user;
        var sql = `UPDATE user SET user_password = MD5(${conn.escape(password)}) , cabang = ${conn.escape(distributor)} , sip = ${conn.escape(no_handphone)} WHERE user_id = ${conn.escape(id)};`
        console.log(sql)
        conn.query(sql, (err, result) => { 
            if (err) throw err
            res.send(result)
        })
    }, laporan_data_sales: (req, res) => {
        var tgl_action_start = req.body.tgl_action_start
        var tgl_action_end = req.body.tgl_action_end
        var { cek_action, cek_order } = req.body
        var tgl_order_start = req.body.tgl_order_start
        var tgl_order_end = req.body.tgl_order_end
        var status = req.body.status | ""
        var sql


        if (!cek_action && cek_order) {
            sql = laporan_data_sales_model.get_from_order(conn.escape(tgl_order_start), conn.escape(tgl_action_end), conn.escape(status))
        } else if (cek_action && !cek_order) {
            sql = laporan_data_sales_model.get_from_action(conn.escape(tgl_action_start), conn.escape(tgl_action_end), conn.escape(status))
        } else if (cek_action && cek_order) {
            sql = laporan_data_sales_model.get_from_order_and_action(conn.escape(tgl_action_start), conn.escape(tgl_action_end), conn.escape(tgl_order_start), conn.escape(tgl_order_end), conn.escape(status))
        } else {
            sql = laporan_data_sales_model.get_from_order_and_action(conn.escape(tgl_action_start), conn.escape(tgl_action_end), conn.escape(tgl_order_start), conn.escape(tgl_order_end), conn.escape(status))
        }

        var main = sql.main_data
        var time_track = sql.time_track
        // res.send(sql)
        conn.query(main, (err, result) => {
            if (err) throw err
            var hasil_main_data = [];
            result.forEach(element => {
                let response = {
                    'id': element.id,
                    'account_number': element.account_number,
                    'name': element.name,
                    'address_home': element.address_home,
                    'address_office': element.address_office,
                    'address_econ': element.address_econ,
                    'phone_home': element.phone_home,
                    'phone_office': element.phone_office,
                    'phone_econ': element.phone_econ,
                    'hp': element.hp,
                    'zip_code': element.zip_code,
                    'cycle': element.cycle,
                    'od': element.od,
                    'balance': element.balance,
                    'total_due': element.total_due,
                    'min_pay': element.min_pay,
                    'batch': element.batch,
                    'produk': element.produk,
                    'users': element.users,
                    'status_aplikasi': element.status_aplikasi,
                    'tgl_order': element.tgl_order,
                    'date_last_action': element.date_last_action,
                    'amount_last_action': element.amount_last_action,
                    'memo_last_action': element.memo_last_action
                }
                hasil_main_data.push(response)
            });
            conn.query(time_track, (err, result_time) => {
                hasil_time = []
                console.log(result_time)
                result_time.forEach(element => {
                    $detail2 = {
                        'account_number': element.account_number,
                        'visit_to': element.visit_to,
                        'date': element.date,
                        'result': element.result,
                        'nominal': element.nominal,
                        'slip_number': element.slip_number,
                        'memo': element.memo,
                        'datetime': element.datetime
                    }
                    hasil_time.push($detail2)
                });
                res.send({ time: hasil_time, main: hasil_main_data })
            })
            // res.send(hasil_main_data)


        })
    }, report_absensi: (req, res) => {
        var { tanggal_start, tanggal_end, nama_agent } = req.body
        laporan_agent.get_report_absensi(tanggal_end, tanggal_start, nama_agent, (result) => res.send(result));
    }, report_absensi_xlsx: (req, res) => {
        var { tanggal_start, tanggal_end, nama_agent } = req.body
        laporan_agent.get_report_absensi(tanggal_end, tanggal_start, nama_agent, (result) => {
            var hasil = []
            result.forEach(element => {
                var cuurrentDate = new Date(element.datetime).toLocaleDateString()
                currentTime = new Date(element.datetime).toLocaleTimeString()
                hasil.push(
                    {
                        user: element.users,
                        tanggal: cuurrentDate + " " + currentTime,
                        absensi: element.result,
                        keterangan: element.result == "Absensi AI" ? "IN" : "OUT"
                    }
                )
            });
            // res.send(hasil)
            res.xls("Laporan Absensi.xlsx", hasil)
        });
    }, report_daily: (req, res) => {
        var { distributor, nama_agent } = req.body
        var { tgl_start, tgl_end } = req.body
        laporan_agent.get_report_daily(tgl_end, tgl_start, nama_agent, distributor, (result) => res.send(result));
    }, report_daily_excel: (req, res) => {
        var { distributor, nama_agent } = req.body
        var { tgl_start, tgl_end } = req.body
        laporan_agent.get_report_daily(tgl_end, tgl_start, nama_agent, distributor, (result) => res.send( result));
    }, report_kunjungan: (req, res) => {
        var { tgl_start, tgl_end } = req.body
        laporan_agent.get_report_kunjungan(tgl_start, tgl_end, (result) => res.send(result));
    }, report_kunjungan_excel: (req, res) => {
        var { tgl_start, tgl_end } = req.body
        var sql_kunjungan = `SELECT 
        date_last_action as Date,users,sisa_tenor as "nama toko",merk as "LE CODE",CONCAT(address_home,",",address_office) as "Titik Koordinat",
        address_econ as "alamat toko",message_last as PJP , produk as TOKO ,name as Distributor,hp as "Besar Toko",od as "Lokasi Toko"
        ,min_pay as PLANG,phone_home as "Ada parkir",phone_econ as "ada kulkas"
        ,last_action as install,salary as "Keterangan No Install" , memo
         FROM main_data WHERE tgl_status >= ${conn.escape(tgl_start)} AND tgl_status <= ${conn.escape(tgl_end)}`
        console.log(sql_kunjungan);
        conn.query(sql_kunjungan, (err, result) => {
            if (err) throw err
            res.xls('laporan kunjungan.xlsx', result);
            // res.send(result)
        })
    }, get_user_from_distributor: (req, res) => {
        var { distributor } = req.body

        var sql = `SELECT DISTINCT users FROM main_data WHERE name = ${conn.escape(distributor)}`
        if (distributor.toLocaleLowerCase() == "all") {
            sql = `SELECT DISTINCT users FROM main_data`
        }
        console.log(sql)
        conn.query(sql, (err, result) => {
            if (err) throw err
            var hasil = Array.from(result, value => value.users)
            res.send(hasil)
        })
    }
    // ,readMedia:(req,res)=> {
    //     var filename = req.params.filename;
    //     var esc_name = conn.escape(filename)
    //     var sql = `SELECT * FROM media WHERE filename = ${esc_name}`
    //     conn.query(sql,(err,result)=>{
    //         if(err) throw err
    //         if(result){
    //             var readStream = fs.createReadStream(`uploads/${filename}`);
    //             readStream.pipe(res)
    //         }
    //     })
    // }
}





