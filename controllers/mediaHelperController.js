const conn = require('../database');
// const excel = require('../helper/excel_to_json')
var fs = require('fs');

module.exports = {
    uploadController: (req, res) => {

        if (req.file == undefined) {
            res.send({ status: "fail" })

        } else {
            var path = req.file.filename
            var key = { path }
            // res.send(excel("uploads/"+path))
            let dateNow = new Date().toLocaleTimeString()
            console.log(dateNow)
            // let today = dateNow.toISOString().split('T')[0] +" " + dateNow.toISOString().split('T')[1].slice(0,8)
            var sql = `INSERT INTO media (filename) VALUES ('${path}')`;
            console.log(sql)
            conn.query(sql, key, (err, result) => {
                if (err) throw err;
                res.send({ status: "sukses", path: req.file.filename, res: result })
            })
        }
    },
    deleteMedia: (req, res) => {
        var name = req.body.filename;

        var sqlDelete = `DELETE FROM media WHERE filename = '${conn.escape(name)}'`
        console.log(sqlDelete)
        conn.query(sqlDelete, (err, result) => {
            console.log(result.affectedRows)
            if (result.affectedRows > 0) {
                fs.unlink("uploads/" + name, (err) => {
                    if (!err) {
                        res.send({ status: "sukses" })
                    } else {
                        res.send(err)
                    }
                })
            } else {
                res.send({ status: "fail" })
            }
        })
    },
    readMedia: (req, res) => {
        var { awal, akhir } = req.body
        var sql = `SELECT filename FROM media WHERE time_stamp <= ${conn.escape(akhir)} AND time_stamp >= ${conn.escape(awal)}`
        conn.query(sql, (err, result) => {
            if (err) throw err
            // console.log(result)
            var key = []
            result.forEach(element => {
                key.push(req.hostname+":"+"2019"+"/" + element.filename)
            });
            res.send(key)


        })
    }

}