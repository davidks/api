const authController = require('./authController.js');
const masterController = require('./masterController');
const mediaHelperController = require("./mediaHelperController")

module.exports = {
    authController,
    masterController,
    mediaHelperController,
}