var conn = require("../database")



function getStatusSQL(status){
    if(!status)
   return `AND (main_data.last_action = ${status}) OR (time_track.result = ${status}) `
   else
   return ""
}
module.exports = {
    // mian data false time track true
    get_from_action(tgl_action_start,tgl_action_end,status){
     
        return{
          
            main_data : `SELECT DISTINCT main_data.* FROM main_data INNER JOIN time_track ON main_data.account_number= time_track.account_number  and main_data.tgl_order=time_track.time WHERE time_track.date >=  ${(tgl_action_start)}  AND time_track.date <=${(tgl_action_end)}`,
            time_track : `SELECT DISTINCT * FROM main_data LEFT JOIN time_track  ON main_data.account_number= time_track.account_number and main_data.tgl_order=time_track.time WHERE time_track.date >=  ${(tgl_action_start)} AND time_track.date <=${(tgl_action_end)} ${getStatusSQL(status)}`,
        }
    },
    // main 2
    get_from_order(tgl_order_start,tgl_order_end,status){
        return{
            main_data : `SELECT DISTINCT * FROM main_data where tgl_order >= ${tgl_order_start} AND tgl_order <= ${tgl_order_end}`,
            time_track : `SELECT DISTINCT * FROM main_data LEFT OUTER JOIN time_track ON main_data.account_number=time_track.account_number  where main_data.tgl_order >= ${tgl_order_start} AND main_data.tgl_order <= ${tgl_order_end}  ${getStatusSQL(status)}`
        }
    },get_from_order_and_action(tgl_action_start,tgl_action_end,tgl_order_start,tgl_order_end,status){
        return {
            main_data : `SELECT DISTINCT main_data.* FROM main_data  INNER JOIN  time_track ON main_data.account_number=time_track.account_number where main_data.tgl_order >= ${tgl_order_start} AND main_data.tgl_order <= ${tgl_order_end} AND time_track.date >= ${tgl_action_start} AND time_track.date <=${tgl_action_end}`,
            time_track: `SELECT DISTINCT * FROM main_data  LEFT OUTER JOIN time_track ON main_data.account_number=time_track.account_number where main_data.tgl_order >= ${tgl_order_start} AND main_data.tgl_order <= ${tgl_order_end} AND time_track.date>= ${tgl_action_start} AND time_track.date <= ${tgl_action_end}  ${getStatusSQL(status)}`
        }
    }

    
    // main 1

}

