var conn = require("../database")

function get_nominal_select_sql(location,column_name=location,add_query="") {
    return `(SELECT COUNT(*) from main_data WHERE name = @perusahaan AND salary = "${location}" AND  date_last_action = @datenow ${add_query} ) as "${column_name}"`
}
module.exports={
    get_report_absensi(tanggal_end, tanggal_start, nama_agent, onComplete) {
        var sql_absensi = `SELECT time_track.users,time_track.visit_to,time_track.datetime,result FROM time_track WHERE (visit_to = "AO" OR visit_to = "AI")`;
        var sql_date_condition = `date < ${conn.escape(tanggal_end)} AND date > ${conn.escape(tanggal_start)}`;
        var sql_user_condition = `users LIKE ${conn.escape(nama_agent)}`;
        var sql_query = sql_absensi + "AND " + sql_date_condition;
        console.log(nama_agent);
        if (nama_agent.toLowerCase() != 'all') {
            sql_query += " AND " + sql_user_condition;
        }
        conn.query(sql_query, (err, result) => {
            if (err)
                throw err;
            onComplete(result)
            // res.send(result);
        });
    },get_report_daily(tgl_end, tgl_start, nama_agent, distributor, onComplete) {
        var sql = `SELECT @datenow := date_last_action as date, @perusahaan := main_data.name as name,
        (SELECT COUNT(main_data.sisa_tenor) FROM main_data WHERE name = @perusahaan and date_last_action = @datenow)  as toko, main_data.users ,(SELECT COUNT(last_action) from main_data WHERE main_data.last_action="install" AND date_last_action = @datenow AND name = @perusahaan ) as install,
            (SELECT COUNT(last_action) from main_data WHERE main_data.last_action="no install" AND date_last_action = @datenow AND name = @perusahaan ) as NoInstall,`;
        sql += get_nominal_select_sql("Ribet") + "," + get_nominal_select_sql("Tunggu Konfirmasi Owner") + "," + get_nominal_select_sql("Hanya mau setor ke sales")
            + "," + get_nominal_select_sql("Order sedikit/jarang") + "," + get_nominal_select_sql("Reason Lainnya") + "," + get_nominal_select_sql("sysmtem down")
            + "," + get_nominal_select_sql("Toko sedang sibuk/tutup - Revisit") + "," + get_nominal_select_sql("Tunggu informasi sales")
            + "," + get_nominal_select_sql("Sudah tua/gaptek")+","+get_nominal_select_sql("","Alasan lainya",'AND last_action != "Install"');
        var where_date_query = `WHERE main_data.date_last_action <=${conn.escape(tgl_end)} AND main_data.date_last_action >=${conn.escape(tgl_start)}`;
        var where_distributor = `AND main_data.users = ${conn.escape(nama_agent)} `;
        var where_agent = `AND main_data.name = ${conn.escape(distributor)} `;
        if (nama_agent.toLowerCase() == 'all') {
            where_agent = "";
        }
        if (distributor.toLowerCase() == 'all') {
            where_distributor = "";
        }
        var sql_from = " FROM main_data ";
        var sql_group = ` GROUP BY date_last_action`;
        // console.log(sql+sql_from+where_date_query+where_distributor+where_agent + sql_group)
        conn.query(sql + sql_from + where_date_query + where_distributor + where_agent + sql_group, (err, result) => {
            if (err)
                throw err;
            onComplete(result);
        });
    }, get_report_kunjungan(tgl_start, tgl_end, onComplete) {
        var sql_kunjungan = `SELECT date,users,slip_number as "nama toko",result,account_number,nominal as "alasan" FROM time_track WHERE visit_to = 
            "TOKO" AND time_track.date >= ${conn.escape(tgl_start)} AND ${conn.escape(tgl_end)}`;
        conn.query(sql_kunjungan, (err, result) => {
            if (err)
                throw err;
            onComplete(result);
        });
    }
    
}