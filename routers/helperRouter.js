var express = require('express');
var router = express.Router();
const { mediaHelperController } = require('../controllers');
const {get_location} = require("../controllers/gpsHelper")
const storage = require("../config/storage")

router.post('/upload',storage.upload.single('file'), mediaHelperController.uploadController);
router.post('/delete',mediaHelperController.deleteMedia);
router.post('/read',mediaHelperController.readMedia)
router.post('/gps',get_location)
module.exports = router;