var express = require('express');
var router = express.Router();
const { masterController } = require('../controllers');


router.get('/principle', masterController.masterPrinciple);
router.get('/user', masterController.masterUser)
router.get('/area', masterController.masterArea);
router.get('/distributor', masterController.masterDistributor);
router.post('/addprinciple', masterController.addPrinciple);
router.post('/deleteprinciple/:id', masterController.deletePrinciple);
router.post('/editprinciple/:id', masterController.editPrinciple);
router.post('/newarea', masterController.newArea);
router.post('/deletearea/:id' , masterController.deleteArea);
router.post('/editarea/:id', masterController.editArea);
router.post('/newdistri', masterController.newDistributor);
router.post('/deletedistri/:id', masterController.deleteDistributor);
router.post('/editdistri/:id', masterController.editDistributor);
router.post('/addUser',masterController.addUser)
router.post('/deleteUser',masterController.userDelete)
router.post('/updateUser',masterController.userUpdate)
router.post('/laporan',masterController.laporan_data_sales)
router.post('/absensi',masterController.report_absensi)
router.post('/daily',masterController.report_daily)
router.post('/kunjungan',masterController.report_kunjungan)
router.post('/agent_distributor',masterController.get_user_from_distributor)
// excel report
router.post('/kunjungan/excel',masterController.report_kunjungan_excel)
router.post('/absensi/excel',masterController.report_absensi_xlsx)
router.post('/daily/excel',masterController.report_daily_excel)
// router.get('/readMedia/:filename',masterController.readMedia)
module.exports = router;