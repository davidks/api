const authRouter = require('./authRouter');
const masterRouter = require('./masterRouter');
const mediaHelper = require('./helperRouter')

module.exports = {
    authRouter,
    masterRouter,
    mediaHelper
}