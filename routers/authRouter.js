var express = require('express');
var router = express.Router();
const { authController } = require('../controllers');

router.post('/login', authController.login);
router.post('/keeplogin',authController.keeplogin);
router.post('/logout',authController.logout);

module.exports = router;