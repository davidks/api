var session = require('express-session')
module.exports = session({
  secret: 'session',
  resave: true,
  saveUninitialized: true,
  cookie: {
    // maxAge: 6000
  }

})